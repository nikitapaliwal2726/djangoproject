from django.shortcuts import render


posts = [
	{
		'author': 'Nikita',
		'title': 'Blog Post 1',
		'content': 'First Post Content'

	},
	{
		'author': 'CoreyMS',
		'title': 'Blog Post 2',
		'content': 'Second Post Content'
	}
]

def home(request):
    context = {
        'posts': posts
    }
    return render(request,'blog/home.html', context)

def about(request):
    return render(request,'blog/about.html', {'title': 'About'})

